package ro.obrejap.maze.view;

import ro.obrejap.maze.Cell;
import ro.obrejap.maze.LabyrinthModel;

public interface LabyrinthView {
	public String labyrinthToString(LabyrinthModel labyrinth);
	
	public String cellToString(Cell cell);
}
