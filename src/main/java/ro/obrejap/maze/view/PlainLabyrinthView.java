package ro.obrejap.maze.view;

import ro.obrejap.maze.Cell;
import ro.obrejap.maze.LabyrinthModel;

public class PlainLabyrinthView implements LabyrinthView{

	@Override
	public String labyrinthToString(LabyrinthModel labyrinth) {
		StringBuilder stringBuilder = new StringBuilder();
		for(int x=0;x<labyrinth.getRowCount();x++){
			for(int y=0;y<labyrinth.getColumnCount();y++){
				Cell cell = labyrinth.getCellAt(x, y);
				stringBuilder.append(cellToString(cell)).append(" ");
			}
			stringBuilder.append("\n");
		}
		return stringBuilder.toString();
	}

	@Override
	public String cellToString(Cell cell) {
		switch(cell.getValue()){
		case FREE:
			return " ";
		case FINISH:
			return "F";
		case START:
			return "S";
		case WALL:
			return "*";
		default:
			throw new IllegalStateException(cell.getValue()+" not handled.");
		}
	}
}
