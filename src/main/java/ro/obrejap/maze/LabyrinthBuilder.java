package ro.obrejap.maze;

import java.util.Set;

import ro.obrejap.maze.Cell.CellValue;


public class LabyrinthBuilder {
	private int[][] labyrinthMatrix;
	
	private LabyrinthBuilder(){		
	}
	
	public static LabyrinthBuilder aLabyrinth(){
		return new LabyrinthBuilder();
	}
	
	public LabyrinthBuilder withMatrix(int[][] labyrinthMatrix){
		this.labyrinthMatrix = labyrinthMatrix;
		return this;
	}
	
	public Labyrinth build(){
		Labyrinth labyrinth = new Labyrinth();
		if(labyrinthMatrix == null){
			throw new IllegalStateException("A labyrinth can't be build without a starting matrix.");
		}
		int rowCount = labyrinthMatrix.length;
		int columnCount = labyrinthMatrix[0].length;
		
		labyrinth.setDimensions(rowCount, columnCount);
		
		for (int i = 0; i < rowCount; i++) {
			for (int j = 0; j < columnCount; j++) {
				int value = labyrinthMatrix[i][j];
				CellValue cellValue = CellValue.getCellBasedOnValue(value);
				Cell cell = new Cell(i, j, cellValue);
				labyrinth.setCell(cell);				
			}
		}
		buildNeighboursRelations(labyrinth);
		return labyrinth;
	}
	
	private void buildNeighboursRelations(Labyrinth labyrinth){
		Set<Cell> cells = labyrinth.getCells();
		for(Cell cell: cells){
			if(cell.isFreeCell()){
				Set<Coordinate> neighbourCoordinates = cell.getCoordinate().getNeighbourCoordinates();
				for (Coordinate neighbourCoordinate : neighbourCoordinates) {
					Cell neighbourCell = labyrinth.getCellAt(neighbourCoordinate);
					if (neighbourCell != null) {
						cell.addNeighbour(neighbourCell);
					}
				}
			}
		}
	}
}
