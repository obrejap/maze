package ro.obrejap.maze.util;

import static ro.obrejap.maze.LabyrinthBuilder.aLabyrinth;
import ro.obrejap.maze.LabyrinthModel;
import ro.obrejap.maze.observer.CompassObserver;
import ro.obrejap.maze.observer.VerboseObserver;
import ro.obrejap.maze.solver.DFS;
import ro.obrejap.maze.solver.LabyrinthSolver;
import ro.obrejap.maze.view.LabyrinthView;
import ro.obrejap.maze.view.PlainLabyrinthView;

public class LabyrinthMain {

	public static void main(String[] args) {
		int[][] labyrinthMatrix = new int[][]{
				{-1,1,1,1,1},
				 {0,0,0,0,1},
				 {1,0,1,0,1},
				 {1,0,0,0,1},
				 {1,1,2,1,1}};
		LabyrinthModel labyrinthModel = aLabyrinth().withMatrix(labyrinthMatrix).build(); 
		
		LabyrinthView labyrinthView = new PlainLabyrinthView();
		System.out.print(labyrinthView.labyrinthToString(labyrinthModel));
		
//		LabyrinthSolver labyrinthSolver = new BFS();
		LabyrinthSolver labyrinthSolver = new DFS();
		
		labyrinthSolver.addObserver(new VerboseObserver());
		labyrinthSolver.addObserver(new CompassObserver());
		
		labyrinthSolver.resolveLabyrinth(labyrinthModel);
	}
}
