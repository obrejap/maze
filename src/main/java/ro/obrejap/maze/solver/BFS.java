package ro.obrejap.maze.solver;

import java.util.LinkedList;
import java.util.List;

import ro.obrejap.maze.Cell;

public class BFS extends AbstractLabyrinthSolver {

	private List<Cell> cellsToExplore;

	public BFS() {
		cellsToExplore = new LinkedList<Cell>();
	}

	@Override
	protected void addCellToBeExplored(Cell cell) {
		cellsToExplore.add(cell);

	}

	@Override
	protected Cell getNextCellToBeExplored() {
		return ((cellsToExplore.size() > 0)) ? cellsToExplore.remove(0) : null;
	}

}
