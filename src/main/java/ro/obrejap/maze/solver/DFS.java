package ro.obrejap.maze.solver;

import java.util.Stack;

import ro.obrejap.maze.Cell;

public class DFS extends AbstractLabyrinthSolver{
	
	private Stack<Cell> cellsToExplore;
	
	public DFS() {
		cellsToExplore = new Stack<Cell>();
	}

	@Override
	protected void addCellToBeExplored(Cell cell) {
		cellsToExplore.push(cell);
		
	}

	@Override
	protected Cell getNextCellToBeExplored() {
		return ((cellsToExplore.size() > 0)) ? cellsToExplore.pop() : null;
	}

}
