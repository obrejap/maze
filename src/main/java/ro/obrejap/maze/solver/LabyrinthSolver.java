package ro.obrejap.maze.solver;

import java.util.List;

import ro.obrejap.maze.Cell;
import ro.obrejap.maze.LabyrinthModel;
import ro.obrejap.maze.observer.LabyrinthObserver;

public interface LabyrinthSolver {
	List<Cell> resolveLabyrinth(LabyrinthModel labyrinthModel);
	
	void addObserver(LabyrinthObserver observer);
}
