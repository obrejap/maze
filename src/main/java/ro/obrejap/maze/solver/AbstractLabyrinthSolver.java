package ro.obrejap.maze.solver;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import ro.obrejap.maze.Cell;
import ro.obrejap.maze.LabyrinthModel;
import ro.obrejap.maze.observer.LabyrinthObserver;

public abstract class AbstractLabyrinthSolver implements LabyrinthSolver {

	private Set<Cell> exploredCells = new HashSet<Cell>();
	private Map<Cell, Cell> previousCell = new HashMap<Cell, Cell>();
	
	private List<LabyrinthObserver> observers;
	
	protected AbstractLabyrinthSolver(){
		exploredCells = new HashSet<Cell>();
		previousCell = new HashMap<Cell, Cell>();
		observers = new LinkedList<LabyrinthObserver>();
	}
	
	//A template method, defining the order in which other methods will be called
	//Some called methods are abstract and will have different implementations
	@Override
	public final List<Cell> resolveLabyrinth(LabyrinthModel labyrinthModel){
		setStartingCell(labyrinthModel.getStartCell());
		Cell currentCell = getNextCellToExplore();
		do{
			processCell(currentCell);
			if(currentCell.isFinishCell()){
				List<Cell> pathFromStart = buildPathFromStartToCell(currentCell);
				publishSolution(pathFromStart);
				return pathFromStart;
			} else {
				addNeighboursCellsToExplore(currentCell);
			}
			currentCell = getNextCellToExplore();			
		}
		while(currentCell!=null);
		return Collections.emptyList();
	}	
	
	@Override
	public void addObserver(LabyrinthObserver observer) {
		observers.add(observer);
	}

	private void setStartingCell(Cell cell){
		addCellToBeExplored(cell);
	}

	private void addNeighboursCellsToExplore(Cell cell) {
		for(Cell neighbour: cell.getNeighbours()){
			if(!exploredCells.contains(neighbour) && neighbour.isFreeCell()){
				addCellToBeExplored(neighbour);
				previousCell.put(neighbour, cell);
			}
		}		
	}

	private Cell getNextCellToExplore() {
		Cell nextCell = getNextCellToBeExplored();
		if (nextCell != null) {
			if (!exploredCells.contains(nextCell)) {				
				exploredCells.add(nextCell);
				return nextCell;
			} else
				return getNextCellToExplore();
		}
		return null;
	}
	
	private void processCell(Cell cell){
		for(LabyrinthObserver observer: observers){
			observer.observeProcessedCell(cell);
		}
	}
	
	private List<Cell> buildPathFromStartToCell(Cell cell){
		List<Cell> pathToStart = new LinkedList<Cell>();
		Cell currentCell = cell;
		do{
			pathToStart.add(0, currentCell);
			currentCell = previousCell.get(currentCell);
		} while (!currentCell.isStartCell());
		pathToStart.add(0, currentCell);
		return pathToStart;
	}
	
	private void publishSolution(List<Cell> pathToStart){
		//using an "unmodifiableList" before passing it to observer
		List<Cell> unmodifiablePathToStart = Collections.unmodifiableList(pathToStart);
		for(LabyrinthObserver observer: observers){
			observer.observeSolution(unmodifiablePathToStart);
		}
	}
	
	//These protected methods will define the algorithm (a DFS or a BFS walk-through)
	protected abstract void addCellToBeExplored(Cell cell);
	
	protected abstract Cell getNextCellToBeExplored();

}
