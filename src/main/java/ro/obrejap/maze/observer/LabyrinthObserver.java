package ro.obrejap.maze.observer;

import java.util.List;

import ro.obrejap.maze.Cell;

public interface LabyrinthObserver {
	void observeProcessedCell(Cell cell);
	
	void observeSolution(List<Cell> pathToStart);
}
