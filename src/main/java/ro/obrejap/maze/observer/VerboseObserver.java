package ro.obrejap.maze.observer;

import java.util.List;

import ro.obrejap.maze.Cell;

public class VerboseObserver implements LabyrinthObserver {

	@Override
	public void observeProcessedCell(Cell cell) {
		System.out.print(" <+> " + cell.getX()+"-"+cell.getY());
	}

	@Override
	public void observeSolution(List<Cell> pathToStart) {
		StringBuilder solution = new StringBuilder("\nA solution was found:\n");
		for(Cell cell:pathToStart){			
			solution.append(cell);
			if(!cell.isFinishCell()){
				solution.append("->");
			}
		}
		System.out.println(solution);
	}

}
