package ro.obrejap.maze.observer;

import java.util.Date;
import java.util.List;

import ro.obrejap.maze.Cell;

public class CompassObserver implements LabyrinthObserver {
	
	@Override
	public void observeProcessedCell(Cell cell) {		
	}

	@Override
	public void observeSolution(List<Cell> pathToStart) {
		Cell previousCell = null;
		for(Cell currentCell:pathToStart){
			if(currentCell.isStartCell()){
				System.out.printf("The journey started on a sunny day of %1$tA in the month of %1$tB.\n", new Date());
				System.out.print("The explorer entered the labyrinth, followed the route: ");				
			} else {
				System.out.print(getCardinalPointDirectionOfCell(previousCell, currentCell)+" ");
				if(currentCell.isFinishCell()){
					System.out.println("\n and finally found what he was looking for: the entire collection of \"Famous explorers\" magazine!");
				}
			}
			previousCell = currentCell;
		}
	}
	
	private CardinalPoint getCardinalPointDirectionOfCell(Cell fromCell, Cell toCell){
		int x = fromCell.getX()-toCell.getX(), y = fromCell.getY() - toCell.getY();
		return CardinalPoint.getRelativeCardinalPointBasedOnValues(x, y);
	}
	
	private enum CardinalPoint{
		N,
		E,
		S,
		W;
		
		public static CardinalPoint getRelativeCardinalPointBasedOnValues(int x, int y){
			if(x==0){
				return (y<0)?CardinalPoint.E:CardinalPoint.W;
			} else {
				return (x<0)?CardinalPoint.S:CardinalPoint.N;
			}
			
		}
	}

}
