package ro.obrejap.maze;

public interface LabyrinthModel {
	int getRowCount();

	int getColumnCount();

	boolean isFreeAt(int x, int y);

	boolean isWallAt(int x, int y);
	
	Cell getCellAt(int x, int y);

	Cell getStartCell();

	Cell getFinishCell();
}
