package ro.obrejap.maze;

import java.util.HashSet;
import java.util.Set;


public class Cell {
	private final Coordinate coordinate;
	private final CellValue value;
	
	private Set<Cell> neighbours;

	public Cell(int x, int y, CellValue value) {
		this.coordinate = new Coordinate(x,y);
		this.value = value;
		neighbours = new HashSet<Cell>();
	}

	public int getX() {
		return coordinate.getX();
	}

	public int getY() {
		return coordinate.getY();
	}

	public CellValue getValue() {
		return value;
	}
	
	public Coordinate getCoordinate(){
		return coordinate;
	}
	
	public void addNeighbour(Cell neighbour){
		this.neighbours.add(neighbour);
		neighbour.neighbours.add(this);
	}
	
	public Set<Cell> getNeighbours(){
		return neighbours;
	}
	
	public boolean isFreeCell(){
		return value == CellValue.FREE || value == CellValue.START || value == CellValue.FINISH;
	}
	
	public boolean isFinishCell(){
		return value == CellValue.FINISH;
	}
	
	public boolean isStartCell(){
		return value == CellValue.START;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		result = prime * result + getX();
		result = prime * result + getY();
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cell other = (Cell) obj;
		if (value != other.value)
			return false;
		if (getX() != other.getX())
			return false;
		if (getY() != other.getY())
			return false;
		return true;
	}

	public enum CellValue {
		FREE(0), WALL(1), START(-1), FINISH(2);
		
		private int value;
		
		CellValue(int value){
			this.value = value;
		}
		
		public static CellValue getCellBasedOnValue(int value){
			for(CellValue cellValue: values()){
				if(cellValue.value==value){
					return cellValue;
				}
			}
			throw new IllegalArgumentException("There is no CellValue for "+value);
		}
	}

	@Override
	public String toString() {
		return "Cell [" + coordinate + "," + value + "]";
	}
}
