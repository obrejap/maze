package ro.obrejap.maze;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import ro.obrejap.maze.Cell.CellValue;

public class Labyrinth implements LabyrinthModel {

	private int rowCount;
	private int columnCount;

	private Cell startCell;
	private Cell finishCell;

	private Map<Coordinate, Cell> cells;

	//The constructor has default access; only classes in the same package (i.e. LabyrinthBuilder) can call it
	Labyrinth() {
		cells = new HashMap<Coordinate, Cell>();
	}

	@Override
	public int getRowCount() {
		return rowCount;
	}

	@Override
	public int getColumnCount() {
		return columnCount;
	}

	void setDimensions(int rowCount, int columnCount) {
		this.rowCount = rowCount;
		this.columnCount = columnCount;
	}

	@Override
	public boolean isFreeAt(int x, int y) {
		return getCellAt(x, y).isFreeCell();
	}

	@Override
	public boolean isWallAt(int x, int y) {
		return getCellAt(x, y).getValue() == CellValue.WALL;
	}

	public Cell getCellAt(int x, int y) {
		return getCellAt(new Coordinate(x, y));
	}

	Cell getCellAt(Coordinate coordinate) {
		if (coordinate.getX() < 0 || coordinate.getX() >= rowCount
				|| coordinate.getY() < 0 || coordinate.getY() >= columnCount) {
			return null;
		}
		return cells.get(coordinate);
	}

	void setCell(Cell cell) {
		Coordinate coordinate = new Coordinate(cell.getX(), cell.getY());
		cells.put(coordinate, cell);
		if (cell.isStartCell()) {
			setStartCell(cell);
		} else if (cell.isFinishCell()) {
			setFinishCell(cell);
		}
	}

	@Override
	public Cell getStartCell() {
		return startCell;
	}

	void setStartCell(Cell startCell) {
		if (this.startCell != null) {
			throw new IllegalStateException(
					"This labyrinth already has a start cell defined.");
		}
		this.startCell = startCell;
	}

	@Override
	public Cell getFinishCell() {
		return finishCell;
	}

	void setFinishCell(Cell finishCell) {
		if (this.finishCell != null) {
			throw new IllegalStateException(
					"This labyrinth already has a finish cell defined.");
		}
		this.finishCell = finishCell;
	}

	Set<Cell> getCells() {
		return new HashSet<Cell>(cells.values());
	}
}
